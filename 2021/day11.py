octos = []
with open('input/day11_input') as f:
    for line in f.readlines():
        octos.append([int(o) for o in line.strip()])


def flash(octos, r, c):
    for r2 in range(r - 1, r + 2):
        for c2 in range(c - 1, c + 2):
            if r2 == r and c2 == c:
                continue
            if (0 <= r2 < len(octos) and 0 <= c2 < len(octos[r2])
                and octos[r2][c2] != -1):
                 octos[r2][c2] += 1


def step(octos):
    flashes = 0

    # Increase energy
    for r, row in enumerate(octos):
        octos[r] = [o + 1 for o in row]

    # Flash
    cont = True
    while cont:
        cont = False
        for r in range(len(octos)):
            for c in range(len(octos[r])):
                if octos[r][c] > 9:
                    flash(octos, r, c)
                    octos[r][c] = -1
                    flashes += 1
                    cont = True

    # Reset
    for r, row in enumerate(octos):
        octos[r] = [0 if o == -1 else o for o in row]

    return flashes


def part_1():
    octos_1 = []
    for row in octos:
        octos_1.append([o for o in row])

    flashes = 0
    for i in range(100):
        flashes += step(octos_1)

    print(f'Part 1: {flashes}')


def part_2():
    octos_2 = []
    for row in octos:
        octos_2.append([o for o in row])

    s = 0
    while True:
        s += 1
        if step(octos_2) == len(octos_2) * len(octos_2[0]):
            print(f'Part 2: {s}')
            break


part_1()
part_2()
