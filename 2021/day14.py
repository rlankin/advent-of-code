import math

template = ''
rules = {}
with open('input/day14_input') as f:
    template = f.readline().strip()
    f.readline()
    for line in f.readlines():
        rule = line.strip().split(' -> ')
        rules[rule[0]] = rule[1]


def dict_inc(dct, val, x):
    dct[val] = dct[val] + x if val in dct else x


def polymerize(polymer, rules, steps):
    pairs = {}
    for i in range(len(polymer) - 1):
        pair = polymer[i : i + 2]
        dict_inc(pairs, pair, 1)

    for step in range(steps):
        pairs_new = dict(pairs)
        for pair, n in pairs.items():
            if n == 0 or pair not in rules:
                continue
            dict_inc(pairs_new, pair[0] + rules[pair], n)
            dict_inc(pairs_new, rules[pair] + pair[1], n)
            pairs_new[pair] -= n
        pairs = pairs_new

    return pairs


def count_elements(pairs):
    counts = {}
    for pair, n in pairs.items():
        dict_inc(counts, pair[0], n)
        dict_inc(counts, pair[1], n)
    return {c: math.ceil(counts[c] / 2) for c in counts}


def part_1():
    pairs = polymerize(template, rules, 10)
    counts = count_elements(pairs)
    counts_sorted = sorted(c for p, c in counts.items() if c > 0)
    print(f'Part 2: {counts_sorted[-1] - counts_sorted[0]}')


def part_2():
    pairs = polymerize(template, rules, 40)
    counts = count_elements(pairs)
    counts_sorted = sorted(c for p, c in counts.items() if c > 0)
    print(f'Part 2: {counts_sorted[-1] - counts_sorted[0]}')


part_1()
part_2()
