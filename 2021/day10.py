lines = []
with open('input/day10_input') as f:
    lines = [line.strip() for line in f.readlines()]


BRACKETS = {
    '(': (')', 1),
    '[': (']', 2),
    '{': ('}', 3),
    '<': ('>', 4)
}


def part_1():
    SCORES = {
        ')': 3,
        ']': 57,
        '}': 1197,
        '>': 25137
    }

    score = 0
    for line in lines:
        stack = []
        for c in line:
            if c in BRACKETS:
                stack.append(c)
            else:
                open = stack.pop()
                if (c != BRACKETS[open][0]):
                    score += SCORES[c]
                    break
    print(f'Part 1: {score}')


def part_2():
    scores = []
    for line in lines:
        stack = []
        for c in line:
            if c in BRACKETS:
                stack.append(c)
            else:
                open = stack.pop()
                if (c != BRACKETS[open][0]):
                    stack.clear()
                    break
        if not stack:
            continue
        score = 0
        while stack:
            score *= 5
            score += BRACKETS[stack.pop()][1]
        scores.append(score)
    scores.sort()
    print(f'Part 2: {scores[len(scores) // 2]}')


part_1()
part_2()
