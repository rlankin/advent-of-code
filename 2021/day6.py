fish = [0] * 9
with open('input/day6_input') as f:
    for i in f.readline().strip().split(','):
        fish[int(i)] += 1


def spawn(fish, days):
    fish_c = list(fish)
    for d in range(days):
        s = fish_c[0]
        for i in range(1, 9):
            fish_c[i - 1] = fish_c[i]
        fish_c[8] = s
        fish_c[6] += s
    return fish_c


print(f'Part 1: {sum(spawn(fish, 80))}')
print(f'Part 2: {sum(spawn(fish, 256))}')
