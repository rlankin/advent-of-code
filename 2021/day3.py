report = []
with open('input/day3_input') as f:
    report = [n.strip() for n in f.readlines()]


def build_counter(report):
    counter = [0] * len(report[0])
    for n in report:
        for i in range(len(n)):
            counter[i] += int(n[i])
    return [(1 if c >= len(report) / 2 else 0) for c in counter]


def bit_str_to_decimal(bit_str):
    decimal = 0
    for b in bit_str:
        decimal = (decimal << 1) | int(b)
    return decimal


def part_1():
    counter = build_counter(report)
    gamma = bit_str_to_decimal(''.join(str(c) for c in counter))
    epsilon = bit_str_to_decimal(''.join(str(1 if c == 0 else 0) for c in counter))

    print(f'Part 1: {gamma * epsilon}')


def part_2():
    oxygen = list(report)
    counter = build_counter(oxygen)
    for i in range(len(counter)):
        oxygen = [n for n in oxygen if int(n[i]) == counter[i]]
        if len(oxygen) == 1:
            break
        counter = build_counter(oxygen)

    co2 = list(report)
    counter = build_counter(co2)
    for i in range(len(counter)):
        co2 = [n for n in co2 if int(n[i]) != counter[i]]
        if len(co2) == 1:
            break;
        counter = build_counter(co2)

    print(f'Part 2: {bit_str_to_decimal(oxygen[0]) * bit_str_to_decimal(co2[0])}')


part_1()
part_2()
