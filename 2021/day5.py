lines = []
x_max, y_max = 0, 0
with open('input/day5_input') as f:
    for line_str in f:
        coords = line_str.strip().split(' -> ')
        c1, c2 = coords[0].split(','), coords[1].split(',')
        line = ((int(c1[0]), int(c1[1])), (int(c2[0]), int(c2[1])))
        lines.append(line)
        x_max = max(x_max, line[0][0] + 1, line[1][0] + 1)
        y_max = max(y_max, line[0][1] + 1, line[1][1] + 1)


def init_map():
    map = []
    for i in range(y_max):
        map.append([0] * x_max)
    return map


def get_step(start, end):
    if start == end:
        return 0
    return 1 if start < end else -1


def map_line(map, line):
    x, y = line[0]
    step_x = get_step(line[0][0], line[1][0])
    step_y = get_step(line[0][1], line[1][1])

    while (x - step_x, y - step_y) != line[1]:
        map[y][x] += 1
        x += step_x
        y += step_y


def map_straight_line(map, line):
    if line[0][0] != line[1][0] and line[0][1] != line[1][1]:
        return
    map_line(map, line)


def map_diag_line(map, line):
    if line[0][0] == line[1][0] or line[0][1] == line[1][1]:
        return
    map_line(map, line)


def count_overlap(map):
    overlap = 0
    for row in map:
        overlap += len([x for x in row if x > 1])
    return overlap


def part_1():
    map = init_map()
    for line in lines:
        map_straight_line(map, line)

    print(f'Part 1: {count_overlap(map)}')


def part_2():
    map = init_map()
    for line in lines:
        map_straight_line(map, line)
        map_diag_line(map, line)

    print(f'Part 2: {count_overlap(map)}')


part_1()
part_2()
