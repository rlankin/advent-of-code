draws = []
boards = []
with open('input/day4_input') as f:
    draws = [int(n) for n in f.readline().strip().split(',')]

    for line in f:
        board = []
        for i in range(5):
            board.append([[int(n), False] for n in f.readline().strip().split(' ') if n])
        boards.append(board)


def mark_number(n, board):
    for row in board:
        for col in row:
            if col[0] == n:
                col[1] = True
                return


def check_board(board):
    for row in board:
        if all(col[1] for col in row):
            return True
    for i in range(5):
        if all(row[i][1] for row in board):
            return True
    return False


def board_score(n, board):
    score = 0
    for row in board:
        score += sum(col[0] for col in row if not col[1])
    return score * n


def part_1():
    for n in draws:
        for b in boards:
            mark_number(n, b)
            if check_board(b):
                print(f'Part 1: {board_score(n, b)}')
                return


def part_2():
    boards_rem = list(boards)
    for n in draws:
        for b in boards_rem:
            mark_number(n, b)

        if len(boards_rem) > 1:
            boards_rem = [b for b in boards_rem if not check_board(b)]
            continue
        if check_board(b):
            print(f'Part 2: {board_score(n, b)}')
            return


part_1()
part_2()
