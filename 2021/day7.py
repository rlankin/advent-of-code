crabs = {}
with open('input/day7_input') as f:
    for i_str in f.readline().strip().split(','):
        i = int(i_str)
        crabs[i] = crabs[i] + 1 if i in crabs else 1


def part_1():
    min_fuel = -1
    for i in range(len(crabs)):
        fuel = sum(abs(j - i) * n for j, n in crabs.items())
        min_fuel = min(min_fuel, fuel) if min_fuel != -1 else fuel
    print(f'Part 1: {min_fuel}')


def part_2():
    min_fuel = -1
    for i in range(len(crabs)):
        fuel = sum(sum(range(1, abs(j - i) + 1)) * n for j, n in crabs.items())
        min_fuel = min(min_fuel, fuel) if min_fuel != -1 else fuel
    print(f'Part 2: {min_fuel}')


part_1()
part_2()
