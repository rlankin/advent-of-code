depths = []
with open('input/day1_input') as f:
    depths = [int(d) for d in f.readlines()]


def part_1():
    inc = 0
    for i in range(1, len(depths)):
        if depths[i] > depths[i - 1]:
            inc += 1

    print(f'Part 1: {inc}')


def part_2():
    inc = 0
    window_prev = sum(depths[:3])
    for i in range(3, len(depths) - 2):
        window = sum(depths[i:i + 3])
        if window > window_prev:
            inc += 1
        window_prev = window

    print(f'Part 2: {inc}')


part_1()
part_2()
