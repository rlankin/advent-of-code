entries = []
with open('input/day8_input') as f:
    for line in f.readlines():
        line_sp = line.strip().split('|')
        patterns = line_sp[0].strip().split(' ')
        patterns.sort(key=len)
        entries.append((patterns, line_sp[1].strip().split(' ')))


def number(segments):
    if segments == [0, 1, 2, 4, 5, 6]:
        return '0'
    if segments == [2, 5]:
        return '1'
    if segments == [0, 2, 3, 4, 6]:
        return '2'
    if segments == [0, 2, 3, 5, 6]:
        return '3'
    if segments == [1, 2, 3, 5]:
        return '4'
    if segments == [0, 1, 3, 5, 6]:
        return '5'
    if segments == [0, 1, 3, 4, 5, 6]:
        return '6'
    if segments == [0, 2, 5]:
        return '7'
    if segments == [0, 1, 2, 3, 5, 6]:
        return '9'
    return '8'


def cull(segments, c, exclude):
    for i in range(len(segments)):
        if i not in exclude and c in segments[i]:
            segments[i].remove(c)


def part_1():
    count = 0
    for entry in entries:
        count += len([o for o in entry[1] if len(o) in [2, 4, 3, 7]])
    print(f'Part 1: {count}')


def part_2():
    total = 0

    for entry in entries:
        segments = []
        for i in range(7):
            segments.append([])
        patterns = entry[0]

        added = []

        # 1
        for c in patterns[0]:
            segments[2].append(c)
            segments[5].append(c)
            added.append(c)
        # 7
        for c in patterns[1]:
            if c not in added:
                segments[0].append(c)
                added.append(c)
        # 4
        for c in patterns[2]:
            if c not in added:
                segments[1].append(c)
                segments[3].append(c)
                added.append(c)
        # 0
        for pattern in patterns[6:9]:
            if any(c not in pattern for c in segments[3]):
                for c in pattern:
                    if c not in added:
                        segments[4].append(c)
                        segments[6].append(c)
                        added.append(c)
                if segments[3][0] in pattern:
                    segments[3].remove(segments[3][0])
                    segments[1].remove(segments[1][1])
                else:
                    segments[3].remove(segments[3][1])
                    segments[1].remove(segments[1][0])
                break
        for pattern in patterns[6:9]:
            # 6
            if any(c not in pattern for c in segments[2]):
                if segments[5][0] in pattern:
                    segments[5].remove(segments[5][1])
                    segments[2].remove(segments[2][0])
                else:
                    segments[5].remove(segments[5][0])
                    segments[2].remove(segments[2][1])
            # 9
            if any(c not in pattern for c in segments[4]):
                if segments[6][0] in pattern:
                    segments[6].remove(segments[6][1])
                    segments[4].remove(segments[4][0])
                else:
                    segments[6].remove(segments[6][0])
                    segments[4].remove(segments[4][1])
        segments = [s[0] for s in segments]

        entry_str = ''
        for digit in entry[1]:
            entry_str += number(sorted(segments.index(c) for c in digit))

        total += int(entry_str)

    print(f'Part 2: {total}')


part_1()
part_2()
