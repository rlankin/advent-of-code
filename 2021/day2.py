commands = []
with open('input/day2_input') as f:
    commands = [(c[0], int(c[1])) for c in (c.split(' ') for c in f.readlines())]

def part_1():
    coords = [0, 0]
    for c in commands:
        if c[0] == 'forward':
            coords[0] += c[1]
        elif c[0] == 'down':
            coords[1] += c[1]
        elif c[0] == 'up':
            coords[1] -= c[1]

    print(f'Part 1: {coords[0] * coords[1]}')

def part_2():
    coords = [0, 0]
    aim = 0
    for c in commands:
        if c[0] == 'forward':
            coords[0] += c[1]
            coords[1] += aim * c[1]
        elif c[0] == 'down':
            aim += c[1]
        elif c[0] == 'up':
            aim -= c[1]

    print(f'Part 2: {coords[0] * coords[1]}')

part_1()
part_2()
