paper = []
fold_insts = []
with open('input/day13_input') as f:
    x_max, y_max = 0, 0
    dots = []
    while True:
        coords = f.readline().strip()
        if not coords:
            break
        coords = coords.split(',')
        dot = [int(coords[0]), int(coords[1])]
        x_max, y_max = max(x_max, dot[0]), max(y_max, dot[1])
        dots.append(dot)

    for y in range(y_max + 1):
        paper.append([False] * (x_max + 1))
    for dot in dots:
        paper[dot[1]][dot[0]] = True

    for line in f.readlines():
        fold = line.strip()[11:].split('=')
        fold_insts.append([fold[0], int(fold[1])])


def print_paper(paper):
    for row in paper:
        print(''.join('#' if c else '.' for c in row))


def fold(paper, inst):
    if inst[0] == 'y':
        for y in range(inst[1] + 1, len(paper)):
            for x in range(len(paper[y])):
                if paper[y][x]:
                    paper[inst[1] - (y - inst[1])][x] = True
        while len(paper) > inst[1]:
            paper.pop()
    else:
        for y in range(len(paper)):
            for x in range(inst[1] + 1, len(paper[y])):
                if paper[y][x]:
                    paper[y][inst[1] - (x - inst[1])] = True
            paper[y] = paper[y][:inst[1]]


def part_1():
    paper_1 = [list(r) for r in paper]
    for inst in fold_insts[:1]:
        fold(paper_1, inst)

    dots = 0
    for row in paper_1:
        dots += row.count(True)

    print(f'Part 1: {dots}')


def part_2():
    paper_2 = [list(r) for r in paper]
    for inst in fold_insts:
        fold(paper_2, inst)

    print('Part 2:')
    print_paper(paper_2)


part_1()
part_2()
