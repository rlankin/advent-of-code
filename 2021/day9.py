heightmap = []
with open('input/day9_input') as f:
    for line in f.readlines():
        heightmap.append([int(c) for c in line.strip()])


def adjacent(heightmap, r, c):
    adj = []
    if r > 0:
        adj.append((r - 1, c))
    if c < len(heightmap[r]) - 1:
        adj.append((r, c + 1))
    if r < len(heightmap) - 1:
        adj.append((r + 1, c))
    if c > 0:
        adj.append((r, c - 1))
    return adj


def is_low_point(heightmap, r, c):
    return all(heightmap[r][c] < heightmap[a[0]][a[1]] for a in adjacent(heightmap, r, c))


def basin_size(heightmap, r, c, points):
    if (r, c) in points:
        return len(points)
    points.add((r, c))

    for adj in adjacent(heightmap, r, c):
        if heightmap[adj[0]][adj[1]] < 9:
            basin_size(heightmap, adj[0], adj[1], points)

    return len(points)


def part_1():
    risk = 0
    for r in range(len(heightmap)):
        for c in range(len(heightmap[r])):
            if is_low_point(heightmap, r, c):
                risk += heightmap[r][c] + 1

    print(f'Part 1: {risk}')


def part_2():
    basins = []
    for r in range(len(heightmap)):
        for c in range(len(heightmap[r])):
            if is_low_point(heightmap, r, c):
                basins.append(basin_size(heightmap, r, c, set()))
    basins.sort(reverse=True)
    print(f'Part 2: {basins[0] * basins[1] * basins[2]}')


part_1()
part_2()
