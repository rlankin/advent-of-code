caves = {}
with open('input/day12_input') as f:
    for line in f.readlines():
        conns = [c for c in line.strip().split('-')]
        if conns[0] not in caves:
            caves[conns[0]] = []
        caves[conns[0]].append(conns[1])
        if conns[1] not in caves:
            caves[conns[1]] = []
        caves[conns[1]].append(conns[0])


def traverse(caves, paths, curr_path, can_visit):
    if curr_path[-1] == 'end':
        paths.append(curr_path)
        return
    for c in caves[curr_path[-1]]:
        if can_visit(c, curr_path):
            path = list(curr_path)
            path.append(c)
            traverse(caves, paths, path, can_visit)


def part_1():
    paths = []
    traverse(caves, paths, ['start'], lambda c, p: c.isupper() or c not in p)
    print(f'Part 1: {len(paths)}')


def part_2():
    def can_visit(cave, path):
        if cave == 'start':
            return False
        if cave.isupper() or cave == 'end' or cave not in path:
            return True
        return not any(path.count(c) == 2 for c in path if c.islower())

    paths = []
    traverse(caves, paths, ['start'], can_visit)
    print(f'Part 2: {len(paths)}')


part_1()
part_2()
