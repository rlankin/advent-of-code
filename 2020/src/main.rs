extern crate lazy_static;

use std::env;

mod days;

pub use crate::days::*;

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() < 2 {
        println!("Please provide day.");
        return;
    }

    match days::day(&args[1]) {
        Ok(day) => {
            println!("Day {}\n======", args[1]);
            print!("Part 1 - ");
            day.part1();
            print!("Part 2 - ");
            day.part2();
        }
        Err(e) => println!("{}", e),
    }
}
