use super::Day;
use std::fs::File;
use std::io::{BufRead, BufReader};

pub struct Day5;

impl Day for Day5 {
    fn part1(&self) {
        let passes = read_input();
        let max_id = passes
            .iter()
            .map(|p| locate_row(&p) * 8 + locate_col(&p))
            .max()
            .unwrap();

        println!("Answer: {}", max_id);
    }

    fn part2(&self) {
        let passes = read_input();

        let mut seats = vec![(0..8).collect::<Vec<i32>>(); 128];

        for pass in passes {
            let row = locate_row(&pass);
            let col = locate_col(&pass);
            match seats[row as usize].iter().position(|&c| c == col) {
                Some(p) => {
                    seats[row as usize].remove(p);
                }
                None => (),
            }
        }

        for (r, row) in seats.iter().enumerate() {
            println!("{}: {:#?}", r, row);
        }
    }
}

fn locate_row(bin: &String) -> i32 {
    let (mut min, mut max) = (0, 127);
    for i in 0..7 {
        match bin.chars().nth(i).unwrap() {
            'F' => max = (max + min) / 2,
            'B' => min = ((max + min) as f32 / 2.0).ceil() as i32,
            _ => panic!("Oops"),
        }
    }

    min
}

fn locate_col(bin: &String) -> i32 {
    let (mut min, mut max) = (0, 7);
    for i in 7..10 {
        match bin.chars().nth(i).unwrap() {
            'L' => max = (max + min) / 2,
            'R' => min = ((max + min) as f32 / 2.0).ceil() as i32,
            _ => panic!("Oops"),
        }
    }

    min
}

fn read_input() -> Vec<String> {
    let file = File::open("src/input/day5.in").unwrap();
    let reader = BufReader::new(file);

    reader.lines().map(|l| l.unwrap()).collect()
}
