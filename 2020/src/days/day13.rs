use super::Day;
use std::fs::File;
use std::io::{BufRead, BufReader};

pub struct Day13;

impl Day for Day13 {
    fn part1(&self) {
        let notes = read_input();

        let mut ts = notes.0;
        loop {
            for &id in &notes.1 {
                if id != 0 && ts % id == 0 {
                    println!("Answer: {}", (ts - notes.0) * id);
                    return;
                }
            }
            ts += 1;
        }
    }
}

fn read_input() -> (u64, Vec<u64>) {
    let file = File::open("src/input/day13.in").unwrap();
    let reader = BufReader::new(file);

    let mut lines = reader.lines();
    (
        lines.next().unwrap().unwrap().parse::<u64>().unwrap(),
        lines
            .next()
            .unwrap()
            .unwrap()
            .split(",")
            .map(|id| match id {
                "x" => 0,
                _ => id.parse::<u64>().unwrap(),
            })
            .collect(),
    )
}
