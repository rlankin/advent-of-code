use super::Day;
use std::collections::HashSet;
use std::fs::File;
use std::io::{BufRead, BufReader};

pub struct Day6;

impl Day for Day6 {
    fn part1(&self) {
        let groups = read_input();
        let sum: usize = groups
            .iter()
            .map(|g| g.join("").chars().collect::<HashSet<char>>().len())
            .sum();

        println!("Answer: {}", sum);
    }

    fn part2(&self) {
        let groups = read_input();
        let sum: usize = groups
            .iter()
            .map(|g| {
                let mut count = g[0].len();
                for a in g[0].chars() {
                    for i in 1..g.len() {
                        if !g[i].contains(a) {
                            count -= 1;
                            break;
                        }
                    }
                }
                count
            })
            .sum();

        println!("Answer: {}", sum);
    }
}

fn read_input() -> Vec<Vec<String>> {
    let file = File::open("src/input/day6.in").unwrap();
    let reader = BufReader::new(file);

    let mut groups = Vec::new();
    let mut answers = Vec::new();
    for line in reader.lines() {
        let line_str = line.unwrap();
        if line_str == "" {
            groups.push(answers);
            answers = Vec::new();
            continue;
        }
        answers.push(line_str);
    }
    groups.push(answers);

    groups
}
