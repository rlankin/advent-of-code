use super::Day;
use std::fs::File;
use std::io::{BufRead, BufReader};

pub struct Day9;

const PREAMBLE_SIZE: usize = 25;

impl Day for Day9 {
    fn part1(&self) {
        let xmas = read_input();

        println!("Answer: {}", find_invalid(&xmas));
    }

    fn part2(&self) {
        let xmas = read_input();

        let invalid_num = find_invalid(&xmas);
        let (mut i, mut j) = (0, 1);
        let mut sum = xmas[i] + xmas[j];
        while sum != invalid_num {
            j += 1;
            sum += xmas[j];

            while sum > invalid_num {
                sum -= xmas[i];
                i += 1;
            }
        }

        let weakness = xmas[i..j].iter().min().unwrap() + xmas[i..j].iter().max().unwrap();
        println!("Answer: {}", weakness);
    }
}

fn find_invalid(xmas: &Vec<u64>) -> u64 {
    let mut nums = xmas[..PREAMBLE_SIZE].to_vec();
    for i in PREAMBLE_SIZE..xmas.len() {
        if !valid(xmas[i], &nums) {
            return xmas[i];
        }
        nums.remove(0);
        nums.push(xmas[i]);
    }

    0
}

fn valid(n: u64, nums: &Vec<u64>) -> bool {
    for i in 0..nums.len() - 1 {
        for j in i + 1..nums.len() {
            if n == nums[i] + nums[j] {
                return true;
            }
        }
    }
    false
}

fn read_input() -> Vec<u64> {
    let file = File::open("src/input/day9.in").unwrap();
    let reader = BufReader::new(file);

    reader
        .lines()
        .map(|l| l.unwrap().parse::<u64>().unwrap())
        .collect()
}
