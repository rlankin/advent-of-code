use super::Day;
use std::fs::File;
use std::io::{BufRead, BufReader};

pub struct Day3;

impl Day for Day3 {
    fn part1(&self) {
        let map = read_input();
        let trees = count_trees(&map, 3, 1);

        println!("Answer: {}", trees);
    }

    fn part2(&self) {
        let map = read_input();
        let trees = count_trees(&map, 1, 1)
            * count_trees(&map, 3, 1)
            * count_trees(&map, 5, 1)
            * count_trees(&map, 7, 1)
            * count_trees(&map, 1, 2);

        println!("Answer: {}", trees);
    }
}

fn count_trees(map: &Vec<Vec<bool>>, x_step: usize, y_step: usize) -> i64 {
    let (mut x, mut y, mut trees) = (0, 0, 0);

    while y < map.len() {
        if map[y][x] {
            trees += 1;
        }
        x = (x + x_step) % map[y].len();
        y += y_step;
    }

    trees
}

fn read_input() -> Vec<Vec<bool>> {
    let file = File::open("src/input/day3.in").unwrap();
    let reader = BufReader::new(file);
    reader
        .lines()
        .map(|line| line.unwrap().chars().map(|c| c == '#').collect())
        .collect()
}
