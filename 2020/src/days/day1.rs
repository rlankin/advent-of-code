use super::Day;
use std::fs::File;
use std::io::{BufRead, BufReader};

pub struct Day1;

impl Day for Day1 {
    fn part1(&self) {
        let expenses = read_input();

        for i in 0..expenses.len() - 1 {
            for j in i + 1..expenses.len() {
                if expenses[i] + expenses[j] == 2020 {
                    println!("Answer: {}", expenses[i] * expenses[j]);
                    return;
                }
            }
        }
    }

    fn part2(&self) {
        let expenses = read_input();

        for i in 0..expenses.len() - 2 {
            for j in i + 1..expenses.len() - 1 {
                for k in j + 1..expenses.len() {
                    if expenses[i] + expenses[j] + expenses[k] == 2020 {
                        println!("Answer: {}", expenses[i] * expenses[j] * expenses[k]);
                        return;
                    }
                }
            }
        }
    }
}

fn read_input() -> Vec<i32> {
    let file = File::open("src/input/day1.in").unwrap();
    let reader = BufReader::new(file);
    reader
        .lines()
        .map(|e| e.unwrap().parse::<i32>().unwrap())
        .collect()
}
