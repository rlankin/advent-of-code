use super::Day;
use lazy_static::lazy_static;
use regex::Regex;
use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader};

pub struct Day4;

lazy_static! {
    static ref EYE_COLORS: Vec<&'static str> =
        vec!["amb", "blu", "brn", "gry", "grn", "hzl", "oth"];
    static ref HAIR_COLOR_RE: Regex = Regex::new(r"^#[0-9a-f]{6}$").unwrap();
    static ref HEIGHT_RE: Regex = Regex::new(r"^(\d+)(cm|in)$").unwrap();
    static ref PASSPORT_FIELD_RE: Regex = Regex::new(r"(\S+):(\S+)").unwrap();
    static ref PASSPORT_ID_RE: Regex = Regex::new(r"^\d{9}$").unwrap();
}

impl Day for Day4 {
    fn part1(&self) {
        let passports = read_input();
        let valid = passports
            .iter()
            .filter(|p| {
                p.contains_key("byr")
                    && p.contains_key("iyr")
                    && p.contains_key("eyr")
                    && p.contains_key("hgt")
                    && p.contains_key("hcl")
                    && p.contains_key("ecl")
                    && p.contains_key("pid")
            })
            .count();

        println!("Answer: {}", valid);
    }

    fn part2(&self) {
        let passports = read_input();
        let valid = passports.iter().filter(|p| validate(p)).count();

        println!("Answer: {}", valid);
    }
}

fn validate(passport: &HashMap<String, String>) -> bool {
    match passport.get("byr") {
        Some(v) => match v.parse::<i32>() {
            Ok(year) => {
                if year < 1920 || year > 2002 {
                    return false;
                }
            }
            Err(_) => return false,
        },
        None => return false,
    }

    match passport.get("iyr") {
        Some(v) => match v.parse::<i32>() {
            Ok(year) => {
                if year < 2010 || year > 2020 {
                    return false;
                }
            }
            Err(_) => return false,
        },
        None => return false,
    }

    match passport.get("eyr") {
        Some(v) => match v.parse::<i32>() {
            Ok(year) => {
                if year < 2020 || year > 2030 {
                    return false;
                }
            }
            Err(_) => return false,
        },
        None => return false,
    }

    match passport.get("hgt") {
        Some(v) => match HEIGHT_RE.captures(v) {
            Some(c) => {
                let hgt = c[1].parse::<i32>().unwrap();
                if (&c[2] == "cm" && (hgt < 150 || hgt > 193))
                    || (&c[2] == "in" && (hgt < 59 || hgt > 76))
                {
                    return false;
                }
            }
            None => return false,
        },
        None => return false,
    }

    match passport.get("hcl") {
        Some(v) => {
            if !HAIR_COLOR_RE.is_match(v) {
                return false;
            }
        }
        None => return false,
    }

    match passport.get("ecl") {
        Some(v) => {
            if !EYE_COLORS.contains(&v.as_str()) {
                return false;
            }
        }
        None => return false,
    }

    match passport.get("pid") {
        Some(v) => {
            if !PASSPORT_ID_RE.is_match(v) {
                return false;
            }
        }
        None => return false,
    }

    true
}

fn read_input() -> Vec<HashMap<String, String>> {
    let file = File::open("src/input/day4.in").unwrap();
    let reader = BufReader::new(file);

    let mut passports = Vec::new();
    let mut passport = HashMap::new();
    for line in reader.lines() {
        let line_str = line.unwrap();
        if line_str == "" {
            passports.push(passport);
            passport = HashMap::new();
            continue;
        }
        for caps in PASSPORT_FIELD_RE.captures_iter(line_str.as_str()) {
            passport.insert(String::from(&caps[1]), String::from(&caps[2]));
        }
    }
    passports.push(passport);

    passports
}
