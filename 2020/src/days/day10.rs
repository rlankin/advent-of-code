use super::Day;
use std::cmp;
use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader};

pub struct Day10;

impl Day for Day10 {
    fn part1(&self) {
        let mut adapters = read_input();
        adapters.insert(0, 0);
        adapters.push(adapters.last().unwrap() + 3);

        let mut diffs = vec![0, 0, 0, 0];
        for i in 1..adapters.len() {
            diffs[(adapters[i] - adapters[i - 1]) as usize] += 1;
        }

        println!("Answer: {}", diffs[1] * diffs[3]);
    }

    fn part2(&self) {
        let mut adapters = read_input();
        adapters.insert(0, 0);
        adapters.push(adapters.last().unwrap() + 3);

        let mut memo = HashMap::new();

        let total = arrangements(0, &adapters, &mut memo);

        println!("Answer: {}", total);
    }
}

fn arrangements(i: usize, adapters: &Vec<u32>, memo: &mut HashMap<usize, u64>) -> u64 {
    if i == adapters.len() - 1 {
        return 1;
    }

    let mut total = 0;
    for j in 1..cmp::min(4, adapters.len() - i) {
        if adapters[i + j] - adapters[i] <= 3 {
            match memo.get(&(i + j)) {
                Some(a) => total += a,
                _ => {
                    let a = arrangements(i + j, adapters, memo);
                    memo.insert(i + j, a);
                    total += a;
                }
            }
        }
    }

    total
}

fn read_input() -> Vec<u32> {
    let file = File::open("src/input/day10.in").unwrap();
    let reader = BufReader::new(file);

    let mut adapters = reader
        .lines()
        .map(|l| l.unwrap().parse::<u32>().unwrap())
        .collect::<Vec<u32>>();
    adapters.sort_unstable();
    adapters
}
