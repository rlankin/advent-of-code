use super::Day;
use std::fs::File;
use std::io::{BufRead, BufReader};

pub struct Day12;

const EAST: i32 = 0;
const NORTH: i32 = 1;
const WEST: i32 = 2;
const SOUTH: i32 = 3;

fn norm(a: i32) -> i32 {
    let mut a_norm = a % 4;
    while a_norm < 0 {
        a_norm += 4;
    }
    a_norm
}

impl Day for Day12 {
    fn part1(&self) {
        let insts = read_input();

        let mut pos = (0, 0, EAST);
        for inst in insts {
            match inst {
                ('N', d) => pos.1 += d,
                ('S', d) => pos.1 -= d,
                ('E', d) => pos.0 += d,
                ('W', d) => pos.0 -= d,
                ('L', d) => pos.2 = norm(pos.2 + (d / 90)),
                ('R', d) => pos.2 = norm(pos.2 - (d / 90)),
                ('F', d) => match pos.2 {
                    NORTH => pos.1 += d,
                    SOUTH => pos.1 -= d,
                    EAST => pos.0 += d,
                    WEST => pos.0 -= d,
                    _ => println!("Unknown direction"),
                },
                _ => println!("Unknown instruction: {:#?}", inst),
            }
        }

        println!("Answer: {}", pos.0.abs() + pos.1.abs());
    }

    fn part2(&self) {
        let insts = read_input();

        let mut pos = (0, 0);
        let mut wp_off = (10, 1);
        for inst in insts {
            match inst {
                ('N', d) => wp_off.1 += d,
                ('S', d) => wp_off.1 -= d,
                ('E', d) => wp_off.0 += d,
                ('W', d) => wp_off.0 -= d,
                ('L', d) => {
                    for _ in 0..d / 90 {
                        wp_off = (-wp_off.1, wp_off.0);
                    }
                }
                ('R', d) => {
                    for _ in 0..d / 90 {
                        wp_off = (wp_off.1, -wp_off.0);
                    }
                }
                ('F', d) => pos = (pos.0 + (wp_off.0 * d), pos.1 + (wp_off.1 * d)),
                _ => println!("Unknown instruction: {:#?}", inst),
            }
        }

        println!("Answer: {}", pos.0.abs() + pos.1.abs());
    }
}

fn read_input() -> Vec<(char, i32)> {
    let file = File::open("src/input/day12.in").unwrap();
    let reader = BufReader::new(file);

    reader
        .lines()
        .map(|l| {
            let s = l.unwrap();
            (s.chars().next().unwrap(), s[1..].parse::<i32>().unwrap())
        })
        .collect::<Vec<(char, i32)>>()
}
