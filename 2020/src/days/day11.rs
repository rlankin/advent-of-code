use super::Day;
use std::fs::File;
use std::io::{BufRead, BufReader};

pub struct Day11;

impl Day for Day11 {
    fn part1(&self) {
        let mut seats = read_input();

        let mut change = true;
        while change {
            change = false;
            let mut seats_next = Vec::new();
            for y in 0..seats.len() {
                let mut row = Vec::new();
                for x in 0..seats[y].len() {
                    let next = next_state((x, y), &seats);
                    row.push(next);
                    change |= next != seats[y][x];
                }
                seats_next.push(row);
            }
            seats = seats_next;
        }

        let occupied: usize = seats
            .iter()
            .map(|r| r.iter().filter(|&s| s == &'#').count())
            .sum();
        println!("Answer: {}", occupied);
    }

    fn part2(&self) {
        let mut seats = read_input();

        let mut change = true;
        while change {
            change = false;
            let mut seats_next = Vec::new();
            for y in 0..seats.len() {
                let mut row = Vec::new();
                for x in 0..seats[y].len() {
                    let next = next_state_los((x, y), &seats);
                    row.push(next);
                    change |= next != seats[y][x];
                }
                seats_next.push(row);
            }
            seats = seats_next;
        }

        let occupied: usize = seats
            .iter()
            .map(|r| r.iter().filter(|&s| s == &'#').count())
            .sum();
        println!("Answer: {}", occupied);
    }
}

fn next_state(coords: (usize, usize), seats: &Vec<Vec<char>>) -> char {
    let mut state = seats[coords.1][coords.0];
    if state == 'L' {
        if occ_count(coords, seats) == 0 {
            state = '#';
        }
    } else if state == '#' {
        if occ_count(coords, seats) >= 4 {
            state = 'L';
        }
    }
    state
}

fn next_state_los(coords: (usize, usize), seats: &Vec<Vec<char>>) -> char {
    let mut state = seats[coords.1][coords.0];
    if state == 'L' {
        if occ_count_los(coords, seats) == 0 {
            state = '#';
        }
    } else if state == '#' {
        if occ_count_los(coords, seats) >= 5 {
            state = 'L';
        }
    }
    state
}

fn occ_count(coords: (usize, usize), seats: &Vec<Vec<char>>) -> u32 {
    let mut count = 0;
    for y_1 in coords.1 as i32 - 1..coords.1 as i32 + 2 {
        for x_1 in coords.0 as i32 - 1..coords.0 as i32 + 2 {
            if y_1 >= 0
                && y_1 < seats.len() as i32
                && x_1 >= 0
                && x_1 < seats[y_1 as usize].len() as i32
                && (x_1 as usize, y_1 as usize) != coords
                && seats[y_1 as usize][x_1 as usize] == '#'
            {
                count += 1;
            }
        }
    }

    count
}

fn occ_count_los(coords: (usize, usize), seats: &Vec<Vec<char>>) -> u32 {
    let mut count = 0;
    for y_d in -1..2 {
        for x_d in -1..2 {
            if y_d == 0 && x_d == 0 {
                continue;
            }

            let mut c = (coords.0 as i32 + x_d, coords.1 as i32 + y_d);
            while c.1 >= 0
                && c.1 < seats.len() as i32
                && c.0 >= 0
                && c.0 < seats[c.1 as usize].len() as i32
            {
                match seats[c.1 as usize][c.0 as usize] {
                    '#' => {
                        count += 1;
                        break;
                    }
                    'L' => break,
                    _ => (),
                }
                c = (c.0 + x_d, c.1 + y_d);
            }
        }
    }

    count
}

fn read_input() -> Vec<Vec<char>> {
    let file = File::open("src/input/day11.in").unwrap();
    let reader = BufReader::new(file);

    reader
        .lines()
        .map(|l| l.unwrap().chars().collect())
        .collect::<Vec<Vec<char>>>()
}
