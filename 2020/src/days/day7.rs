use super::Day;
use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader};

pub struct Day7;

impl Day for Day7 {
    fn part1(&self) {
        let bags = read_input();
        let count = bags
            .keys()
            .filter(|b| can_contain(&bags, b, &String::from("shiny gold")))
            .count();

        println!("Answer: {}", count);
    }

    fn part2(&self) {
        let bags = read_input();

        println!("Answer: {}", min_bags(&bags, &String::from("shiny gold")));
    }
}

fn can_contain(bags: &HashMap<String, Vec<(u32, String)>>, outer: &String, inner: &String) -> bool {
    for b in bags.get(outer).unwrap() {
        if &b.1 == inner || can_contain(bags, &b.1, inner) {
            return true;
        }
    }

    false
}

fn min_bags(bags: &HashMap<String, Vec<(u32, String)>>, bag: &String) -> usize {
    bags.get(bag)
        .unwrap()
        .iter()
        .map(|b| (b.0 as usize) + (b.0 as usize) * min_bags(bags, &b.1))
        .sum::<usize>()
}

fn read_input() -> HashMap<String, Vec<(u32, String)>> {
    let file = File::open("src/input/day7.in").unwrap();
    let reader = BufReader::new(file);

    let mut bags = HashMap::new();
    for line in reader.lines() {
        let line_u = line.unwrap();
        let mut type_iter = line_u.split(" bags contain ");

        let bag_type = type_iter.next().unwrap().to_string();
        let mut contents = Vec::new();
        for c in type_iter.next().unwrap().split(", ") {
            let mut c_iter = c.split(" ");
            let count = c_iter.next().unwrap();
            if count == "no" {
                break;
            }

            let mut content_type = c_iter.next().unwrap().to_owned();
            content_type.push(' ');
            content_type.push_str(c_iter.next().unwrap());
            contents.push((count.parse::<u32>().unwrap(), content_type));
        }
        bags.insert(bag_type, contents);
    }

    bags
}
