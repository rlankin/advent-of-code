use super::Day;
use std::fs::File;
use std::io::{BufRead, BufReader};

pub struct Day8;

impl Day for Day8 {
    fn part1(&self) {
        let code = read_input();

        println!("Answer: {}", exec(&code).1);
    }

    fn part2(&self) {
        let mut code = read_input();

        for i in 0..code.len() {
            match code[i].0.as_str() {
                "jmp" => code[i].0 = "nop".to_string(),
                "nop" => code[i].0 = "jmp".to_string(),
                _ => continue,
            }

            let res = exec(&code);
            if res.0 {
                println!("Answer: {}", res.1);
                return;
            }

            match code[i].0.as_str() {
                "jmp" => code[i].0 = "nop".to_string(),
                "nop" => code[i].0 = "jmp".to_string(),
                _ => continue,
            }
        }
    }
}

fn exec(code: &Vec<(String, i32)>) -> (bool, i32) {
    let mut a = 0;
    let mut ip = 0;
    let mut visited = Vec::new();

    while (ip as usize) < code.len() {
        if visited.contains(&ip) {
            return (false, a);
        }
        visited.push(ip);

        match (code[ip as usize].0.as_str(), code[ip as usize].1) {
            ("acc", n) => a += n,
            ("jmp", n) => {
                ip += n;
                continue;
            }
            _ => (),
        }
        ip += 1;
    }

    (true, a)
}

fn read_input() -> Vec<(String, i32)> {
    let file = File::open("src/input/day8_test.in").unwrap();
    let reader = BufReader::new(file);

    reader
        .lines()
        .map(|l| {
            let l_u = l.unwrap();
            let mut tokens = l_u.split(" ");
            (
                tokens.next().unwrap().to_string(),
                tokens
                    .next()
                    .unwrap()
                    .replace("+", "")
                    .parse::<i32>()
                    .unwrap(),
            )
        })
        .collect()
}
