use super::Day;
use regex::Regex;
use std::fs::File;
use std::io::{BufRead, BufReader};

pub struct Day2;

impl Day for Day2 {
    fn part1(&self) {
        let passwords = read_input();

        let mut valid = 0;
        for p in passwords {
            if p.3.matches(p.2).count() >= p.0 && p.3.matches(p.2).count() <= p.1 {
                valid += 1;
            }
        }

        println!("Answer: {}", valid);
    }

    fn part2(&self) {
        let passwords = read_input();

        let mut valid = 0;
        for p in passwords {
            let p_chars = p.3.chars().collect::<Vec<char>>();
            if (p_chars[p.0 - 1] == p.2) ^ (p_chars[p.1 - 1] == p.2) {
                valid += 1
            }
        }

        println!("Answer: {}", valid);
    }
}

fn read_input() -> Vec<(usize, usize, char, String)> {
    let re = Regex::new(r"^(\d+)-(\d+) (\D): (\D+)$").unwrap();

    let file = File::open("src/input/day2.in").unwrap();
    let reader = BufReader::new(file);
    reader
        .lines()
        .map(|line| {
            let line_str = line.unwrap();
            let caps = re.captures(line_str.as_str()).unwrap();
            (
                caps[1].parse::<usize>().unwrap(),
                caps[2].parse::<usize>().unwrap(),
                caps[3].chars().next().unwrap(),
                String::from(&caps[4]),
            )
        })
        .collect()
}
