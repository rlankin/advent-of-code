pub mod day1;
pub mod day10;
pub mod day11;
pub mod day12;
pub mod day13;
pub mod day2;
pub mod day3;
pub mod day4;
pub mod day5;
pub mod day6;
pub mod day7;
pub mod day8;
pub mod day9;

pub trait Day {
    fn part1(&self) {
        println!("Part 1 not implemented.");
    }

    fn part2(&self) {
        println!("Part 2 not implemented.");
    }
}

pub fn day(d: &String) -> Result<Box<dyn Day>, &'static str> {
    match d.as_str() {
        "1" => Ok(Box::new(day1::Day1)),
        "2" => Ok(Box::new(day2::Day2)),
        "3" => Ok(Box::new(day3::Day3)),
        "4" => Ok(Box::new(day4::Day4)),
        "5" => Ok(Box::new(day5::Day5)),
        "6" => Ok(Box::new(day6::Day6)),
        "7" => Ok(Box::new(day7::Day7)),
        "8" => Ok(Box::new(day8::Day8)),
        "9" => Ok(Box::new(day9::Day9)),
        "10" => Ok(Box::new(day10::Day10)),
        "11" => Ok(Box::new(day11::Day11)),
        "13" => Ok(Box::new(day13::Day13)),
        _ => Err("Day not implemented."),
    }
}
